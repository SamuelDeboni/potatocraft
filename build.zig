const std = @import("std");
const builtin = @import("builtin");
const Builder = std.build.Builder;

const packages = [_]std.build.Pkg {
    .{
        .name = "zig_tt",
        .path = "packages/zig_truetype/zig_truetype.zig",
    },
    
    .{
        .name = "enet",
        .path = "packages/enet/enet.zig",
    },
};


pub fn build(b: *Builder) void {
    var target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    
    const windows = b.option(bool, "windows", "create windows build") orelse false;
    const strip = b.option(bool, "strip", "strip debug info") orelse false;
    
    {
        var client = b.addExecutable("potatocraft", "src/glfw_plataform.zig");
        
        for (packages) |pack| {
            client.addPackage(pack);
        }
        
        client.addCSourceFile("packages/zig_truetype/stb_truetype.c", &[_][]const u8{"-std=c99"});
        client.setTarget(target);
        
        if (windows) {
            client.setTarget
                (.{
                     .cpu_arch = .x86_64,
                     .os_tag = .windows,
                     .abi = .gnu,
                 });
        }
        
        if (strip) {
            client.strip = true;
        }
        client.setBuildMode(mode);
        client.linkLibC();
        
        if (windows) {
            client.linkSystemLibrary("gdi32");
            client.linkSystemLibrary("shell32");
            client.linkSystemLibrary("kernel32");
            client.linkSystemLibrary("ws2_32");
            client.linkSystemLibrary("winmm");
            client.addLibPath("lib");
        }
        client.addIncludeDir("include");
        client.addIncludeDir("packages/zig_truetype");
        client.addIncludeDir("packages/enet");
        client.linkSystemLibrary("m");
        client.linkSystemLibrary("glfw3");
        client.linkSystemLibrary("enet");
        
        client.install();
        
        const run_cmd = client.run();
        run_cmd.step.dependOn(b.getInstallStep());
        
        const run_step = b.step("run", "Run the app");
        run_step.dependOn(&run_cmd.step);
    }
    
    if (!windows) {
        var server = b.addExecutable("server", "src/server_main.zig");
        
        server.addPackage(packages[1]);
        
        server.setTarget(target);
        
        if (strip) {
            server.strip = true;
        }
        server.setBuildMode(mode);
        
        server.addIncludeDir("packages/enet");
        
        server.linkSystemLibrary("c");
        server.linkSystemLibrary("enet");
        
        server.install();
    }
    
    if (false) {
        var potatel = b.addExecutable("potatel", "src/potatel/potatel_main.zig");
        
        potatel.setTarget(target);
        
        if (strip) {
            potatel.strip = true;
        }
        potatel.setBuildMode(mode);
        
        potatel.linkSystemLibrary("c");
        
        potatel.install();
    }
}
