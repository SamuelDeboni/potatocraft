pub const std = @import("std");

pub usingnamespace @cImport
({
     @cInclude("stb_truetype.h");
 });

pub const Font = struct {
    char_data: []BakedChar = undefined,
    first_char: u32 = 0,
    num_chars: u32 = 0,
    bitmap: []u8,
    bitmap_w: u32 = 0,
    bitmap_h: u32 = 0,
};

pub const BakedChar = stbtt_bakedchar;

pub fn bake_font_bitmap(data: [*:0]const u8, pixel_h: f32, bitmap: []u8, bitmap_w: i32, bitmap_h: i32, first_char: u32, num_chars: u32, char_data: []BakedChar) !void {
    
    const err = stbtt_BakeFontBitmap(@ptrCast([*]const u8, &data[0]), 0, pixel_h, @ptrCast([*]u8, bitmap.ptr), bitmap_w, bitmap_h, @intCast(c_int, first_char), @intCast(c_int, num_chars), @ptrCast([*]BakedChar, char_data.ptr));
    
    std.debug.print("pack return {}\n", .{err});
    if (err < 0) {
        return error.BitMapIsToSmall;
    }
}

const tmp_bitmap_w = 1024;
const tmp_bitmap_h = 1024;
var tmp_bitmap: [tmp_bitmap_w * tmp_bitmap_h]u8 = undefined;

pub fn bake_font_alloc(al: *std.mem.Allocator, font_size: f32, first_char: u32, num_chars: u32, ttf_data: [*:0]const u8) !Font {
    
    var char_data = try al.alloc(BakedChar, num_chars);
    
    const err = stbtt_BakeFontBitmap(@ptrCast([*]const u8, &ttf_data[0]), 0, font_size, @ptrCast([*]u8, &tmp_bitmap[0]),
                                     tmp_bitmap_w, tmp_bitmap_h,
                                     @intCast(c_int, first_char), @intCast(c_int, num_chars), @ptrCast([*]BakedChar, char_data.ptr));
    
    var result = Font {
        .char_data = char_data,
        .num_chars = num_chars,
        .first_char = first_char,
        .bitmap = undefined,
    };
    
    if (err < 0) {
        return error.BitMapIsToSmall;
    } else if (err == 0) {
        result.bitmap_w = tmp_bitmap_w;
        result.bitmap_h = tmp_bitmap_h;
        result.bitmap = try al.alloc(u8, result.bitmap_w * result.bitmap_h);
        for (result.bitmap) |*it, i| it.* = tmp_bitmap[i];
    } else {
        result.bitmap_w = tmp_bitmap_w;
        result.bitmap_h = @intCast(u32, err) + 1;
        result.bitmap = try al.alloc(u8, result.bitmap_w * result.bitmap_h);
        
        _ = stbtt_BakeFontBitmap(@ptrCast([*]const u8, &ttf_data[0]),
                                 0, font_size, @ptrCast([*]u8, result.bitmap.ptr),
                                 @intCast(c_int, result.bitmap_w),
                                 @intCast(c_int, result.bitmap_h),
                                 @intCast(c_int, first_char), @intCast(c_int, num_chars), @ptrCast([*]BakedChar, char_data.ptr));
    }
    
    return result;
}

pub const Quad = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    w: f32 = 0.0,
    h: f32 = 0.0,
    
    u: f32 = 0.0,
    v: f32 = 0.0,
    us: f32 = 0.0,
    vs: f32 = 0.0,
};

pub fn get_baked_quad(char_data: []BakedChar, character: u8, bitmap_w: u32, bitmap_h: u32, x: *f32, y: *f32) Quad {
    
    var q: stbtt_aligned_quad = undefined;
    stbtt_GetBakedQuad(@ptrCast([*]const BakedChar, char_data),
                       @intCast(c_int, bitmap_w),
                       @intCast(c_int, bitmap_h),
                       character, x, y,
                       @ptrCast([*]stbtt_aligned_quad, &q), 1);
    
    return Quad {
        .x = q.x0,
        .y = q.y0,
        .w = q.x1 - q.x0,
        .h = q.y1 - q.y0,
        
        .u = q.s1,
        .v = q.t0,
        .us = q.s0 - q.s1,
        .vs = q.t1 - q.t0,
    };
}