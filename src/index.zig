pub const std = @import("std");
pub const Allocator = std.mem.Allocator;
pub const plt = @import("glfw_plataform.zig");
pub const clamp = std.math.clamp;

pub var g_allocator: *Allocator = undefined;

pub usingnamespace @import("image.zig");
pub usingnamespace @import("player.zig");
pub usingnamespace @import("blocks.zig");
pub usingnamespace @import("vector_math.zig");
pub usingnamespace @import("dutil.zig");
pub usingnamespace @import("chunk.zig");

pub const input =  @import("input.zig");
pub const net = @import("network.zig");
pub const ztt = @import("zig_tt");
pub const render = @import("render_opengl.zig");
pub const dui = @import("duilib.zig");
pub const server = @import("server.zig");
pub const enet = @import("enet");


//~ TODO(samuel): Common stuff
pub const plog = @import("plog.zig");
pub const netdef = @import("netdef.zig");
pub const str = @import("string.zig");



// Globals
pub var chunk_hash_table: ChunkHashTable = undefined;
pub var ui_camera = render.Camera{ .ortogonal = true, .size = 2.0};
pub var tile_tex = render.Texture{};
pub var tile_tex_array = render.TextureArray{};

