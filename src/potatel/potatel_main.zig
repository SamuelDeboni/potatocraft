usingnamespace @import("potatel_index.zig");

pub fn main() !void {
    plog.logln(.potatel, "Hello world");
    _ = try assembly_code(
        \\    jmp MAIN # jump to main
        \\
        \\DATA:
        \\    .data d42, d87, "asdfadsf", x0
        \\
        \\VAR_YAY:
        \\    .data x3, "yay"
        \\
        \\MAIN:
        \\    # load 42 to r1
        \\    mova d42
        \\    movr r1
        \\
        \\    # load 87 to r2
        \\    mova d87
        \\    movr r2
        \\
        \\    call ADD
        \\
        \\    jmp MAIN
        \\
        \\ADD:
        \\    add  r1 r2
        \\    movr r0
        \\    ret
    );
}