usingnamespace @import("potatel_index.zig");

const AssemblyError = error {
    batatinha,
};

// TODO(Ifaresi): use new ISA

const Instruction = enum(u8) {
    nop,  
    add, 
    sub, 
    mul,
    div,
    inc,
    dec,
    mula,
    diva,
    _or,
    _and,
    xor,
    not,
    sl,
    srl,
    sra,
    mova,
    movr,
    load,
    store,
    push,
    pop,
    cmp,
    jmp,
    je,
    jne,
    jgt,
    jlt,
    call,
    ret,
    zero,
    acc,
    ic,
};

fn get_instruction(token: []const u8) !Instruction {
    var ins: Instruction = undefined;
    
    if (str.cmp(token, "zero")) {
        ins = .zero;
    } else if (str.cmp(token, "acc")) {
        ins = .acc;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else if (str.cmp(token, "ic")) {
        ins = .ic;
    } else {
        return error.InstructionNotFound;
    }
    
    return ins;
}

const InstructionMode = enum {
    nothing,
    reg,
    reg_reg,
    imeditate,
    reg_reg_or_reg_bi,
    reg_bi_or_imediate,
    reg_or_imediate,
};

fn get_instruction_mode(instruction: Instruction) InstructionMode {
    var mode: InstructionMode = undefined;
    
    switch (instruction) {
        .nop   => { mode = .nothing;            },
        .add   => { mode = .reg_reg;            }, 
        .sub   => { mode = .reg_reg;            }, 
        .mul   => { mode = .reg_reg;            },
        .div   => { mode = .reg_reg;            },
        .inc   => { mode = .imeditate;          },
        .dec   => { mode = .imeditate;          },
        .mula  => { mode = .imeditate;          },
        .diva  => { mode = .imeditate;          },
        ._or   => { mode = .reg_reg;            },
        ._and  => { mode = .reg_reg;            },
        .xor   => { mode = .reg_reg;            },
        .not   => { mode = .reg;                },
        .sl    => { mode = .reg_reg_or_reg_bi;  },
        .srl   => { mode = .reg_reg_or_reg_bi;  },
        .sra   => { mode = .reg_reg_or_reg_bi;  },
        .mova  => { mode = .reg_or_imediate;    },
        .movr  => { mode = .reg;                },
        .load  => { mode = .reg_bi_or_imediate; },
        .store => { mode = .reg_bi_or_imediate; },
        .push  => { mode = .reg_or_imediate;    },
        .pop   => { mode = .reg_or_imediate;    },
        .cmp   => { mode = .reg_or_imediate;    },
        .jmp   => { mode = .reg_or_imediate;    },
        .je    => { mode = .reg_or_imediate;    },
        .jne   => { mode = .reg_or_imediate;    },
        .jgt   => { mode = .reg_or_imediate;    },
        .jlt   => { mode = .reg_or_imediate;    },
        .call  => { mode = .reg_or_imediate;    },
        .ret   => { mode = .reg_or_imediate;    }, 
        else => {},
    }
    
    return mode;
}

const Register = enum(u8) {
    zero = 0b00000000,
    acc  = 0b10000000,
    ic   = 0b10000001,
    sp   = 0b10000010,
    ra   = 0b10000011,
    r0   = 0b00000001,
    r1   = 0b00000010,
    r2   = 0b00000011,
    r3   = 0b00000100,
    r4   = 0b00000101,
    r5   = 0b00000110,
    r6   = 0b00000111,
    r7   = 0b00001000,
    r8   = 0b00001001,
    r9   = 0b00001010,
};

fn get_register(token: []const u8) !Register {
    var reg: Register = undefined;
    
    if (str.cmp(token, "zero")) {
        reg = .zero;
    } else if (str.cmp(token, "acc")) {
        reg = .acc;
    } else if (str.cmp(token, "ic")) {
        reg = .ic;
    } else if (str.cmp(token, "sp")) {
        reg = .sp;
    } else if (token.len == 2 and token[0] == 'r') {
        switch (token[1]) {
            'a' => { reg = .ra; },
            '0' => { reg = .r0; },
            '1' => { reg = .r1; },
            '2' => { reg = .r2; },
            '3' => { reg = .r3; },
            '4' => { reg = .r4; },
            '5' => { reg = .r5; },
            '6' => { reg = .r6; },
            '7' => { reg = .r7; },
            '8' => { reg = .r8; },
            '9' => { reg = .r9; },
            else => return error.RegisterNotFound,
        }
    } else {
        return error.RegisterNotFound;
    }
    return reg;
}

pub fn assembly_code(code: []const u8) ![]u8 {
    var buffer = code;
    while (str.next_line(&buffer)) |*line| {
        str.trim_spaces(line);
        
        std.debug.print("line: {s}\n", .{line.*});
        
        while (str.next_token(line, ' ')) |*token| {
            str.trim_spaces(token);
            
            const ins = try get_instruction(token.*);
            const mode = get_instruction_mode(ins);
            
            switch (mode) {
                .nothing => {
                    
                },
                
                else => {},
            }
            
            if (token.*[0] == '#') {
                break;
            }
            
            std.debug.print("\ttoken: {s}\n", .{token.*});
        }
    }
    
    var batata = [_]u8{0} ** 9;
    return batata[0..];
}