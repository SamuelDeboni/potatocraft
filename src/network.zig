usingnamespace @import("index.zig");
const assert = std.debug.assert;

var ping_send_timestamp: i64 = 0;
pub var last_server_ping: i64 = 0;

var queue = [_]netdef.RawEvent{.{}} ** 1024;
var queue_size: u32 = 0;
pub var using_queue_mutex = std.Thread.Mutex{};

pub fn next_raw_event() ?netdef.RawEvent {
    if (queue_size > 0) {
        queue_size -= 1;
        return queue[queue_size];
    }
    
    return null;
}

var enet_client: ?*enet.ENetHost = null;
var enet_peer: ?*enet.ENetPeer = null;

var my_sender_code: u8 = 0;

pub fn send_chunk_request_event(pos: Vec3i) void {
    plog.logln(.client, "Sending chunk request event");
    
    const event = netdef.ChunkRequestEvent{
        .sender = my_sender_code,
        .chunk_pos_x = pos.x,
        .chunk_pos_y = pos.y,
    };
    
    const packet = enet.enet_packet_create(@ptrCast([*]const u8, &event), @sizeOf(netdef.ChunkRequestEvent), enet.ENET_PACKET_FLAG_RELIABLE);
    _ = enet.enet_peer_send(enet_peer, 0, packet);
}

pub fn send_block_break_event(chunk: *Chunk, pos: Vec3i) void {
    plog.logln(.client, "Sending block break event");
    
    const event = netdef.BlockBreakEvent{
        .sender = my_sender_code,
        .chunk_pos_x = chunk.position.x,
        .chunk_pos_y = chunk.position.y,
        .block_pos_x = @intCast(u8, pos.x),
        .block_pos_y = @intCast(u8, pos.y),
        .block_pos_z = @intCast(u16, pos.z),
    };
    
    const packet = enet.enet_packet_create(@ptrCast([*]const u8, &event), @sizeOf(netdef.BlockBreakEvent), enet.ENET_PACKET_FLAG_RELIABLE);
    _ = enet.enet_peer_send(enet_peer, 0, packet);
}

pub fn send_block_place_event(chunk: *Chunk, pos: Vec3i, bid: u16) void {
    plog.logln(.client, "Sending block place event");
    
    const event = netdef.BlockPlaceEvent{
        .sender = my_sender_code,
        .block_id = bid,
        .chunk_pos_x = chunk.position.x,
        .chunk_pos_y = chunk.position.y,
        .block_pos_x = @intCast(u8, pos.x),
        .block_pos_y = @intCast(u8, pos.y),
        .block_pos_z = @intCast(u16, pos.z),
    };
    
    const packet = enet.enet_packet_create(@ptrCast([*]const u8, &event), @sizeOf(netdef.BlockPlaceEvent), enet.ENET_PACKET_FLAG_RELIABLE);
    _ = enet.enet_peer_send(enet_peer, 0, packet);
}

fn ping() void {
    ping_send_timestamp = std.time.milliTimestamp();
    var packet_data = [_]u8{0, 'y', 'a', 'y'};
    packet_data[0] = @enumToInt(netdef.EventType.ping);
    const packet = enet.enet_packet_create(packet_data[0..], 4, enet.ENET_PACKET_FLAG_RELIABLE);
    _ = enet.enet_peer_send(enet_peer, 0, packet);
}

fn network_client_loop(args: void) void {
    var event: enet.ENetEvent = undefined;
    
    var do_ping = true;
    
    
    while (true) {
        if (do_ping and (std.time.milliTimestamp() - ping_send_timestamp) >= 1000) {
            ping();
            do_ping = false;
        }
        
        while (enet.enet_host_service(enet_client, &event, 100) > 0) {
            if (do_ping and (std.time.milliTimestamp() - ping_send_timestamp) >= 1000) {
                ping();
                do_ping = false;
            }
            
            switch (event.type) {
                .ENET_EVENT_TYPE_CONNECT => std.debug.print("connection event\n", .{}),
                
                .ENET_EVENT_TYPE_RECEIVE => {
                    const data = event.packet.*.data[0..event.packet.*.dataLength];
                    //plog.loglnfmt(.client, "packet: {s}", .{data});
                    switch (data[0]) {
                        @enumToInt(netdef.EventType.pong) =>  {
                            last_server_ping = std.time.milliTimestamp() - ping_send_timestamp;
                            //plog.loglnfmt(.client, "ping: {} ms", .{last_server_ping});
                            do_ping = true;
                        },
                        
                        @enumToInt(netdef.EventType.chunk_response) => {
                            
                        },
                        
                        else => blk: {
                            plog.logln(.client, "new event");
                            
                            var length: usize = event.packet.*.dataLength;
                            if (length > 63) {
                                plog.loglnfmt(.client, "event size is higher than 63, {} bytes", .{length});
                                length = 63;
                            }
                            
                            const held = using_queue_mutex.acquire();
                            {
                                defer held.release();
                                
                                if (queue_size >= 1024) {
                                    plog.logln(.client, "queue is full!");
                                    break :blk;
                                }
                                
                                queue[queue_size].len = @intCast(u8, length);
                                for (queue[queue_size].data[0..length]) |*d, i| {
                                    d.* = data[i];
                                }
                                queue_size += 1;
                            }
                            
                        },
                    }
                },
                
                .ENET_EVENT_TYPE_DISCONNECT => std.debug.print("disconnect event\n", .{}),
                
                else => std.debug.print("Unknow event type\n", .{}),
            }
        }
    }
}

pub fn connect_to_server(url: [:0]const u8, port: isize) !void {
    if (!server.headless) {
        while (!server.inited) {
            std.time.sleep(std.time.ns_per_ms * 500);
        }
    }
    
    plog.logln(.client, "Initing enet...");
    assert(enet.enet_initialize() == 0);
    
    enet_client = enet.enet_host_create(null, 1, 1, 0, 0);
    assert(enet_client != null);
    
    plog.logln(.client, "Enet inited with success");
    
    var address: enet.ENetAddress = undefined;
    
    _ = enet.enet_address_set_host(&address, url);
    address.port = @intCast(c_ushort, port);
    
    enet_peer = enet.enet_host_connect(enet_client, &address, 1, 0);
    if (enet_peer == null) return error.enetPeerIsNull;
    
    var event: enet.ENetEvent = undefined;
    if (enet.enet_host_service(enet_client, &event, 5000) > 0 and event.type == .ENET_EVENT_TYPE_CONNECT) {
        plog.logln(.client, "Connection succeded");
    } else {
        return error.unableToConnect;
    }
    
    const network_thread = try std.Thread.spawn(network_client_loop, {});
}
