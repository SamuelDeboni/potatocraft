#version 330 core

layout (location = 0) in vec2 pos_xy;
layout (location = 1) in float pos_z;
layout (location = 2) in vec2 uv_in;
layout (location = 3) in vec3 normal;
layout (location = 4) in float tex_pos;

out vec3 f_pos;
out vec2 f_uv;
out float f_tex_pos;
out vec3 f_normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

float tile_size = 1.0 / 32.0;

void main()
{
    vec3 aPos = vec3(pos_xy, pos_z);
    f_normal = normal;
    f_uv = uv_in;
    f_tex_pos = tex_pos;
    
    vec4 position = view * model * vec4(aPos, 1.0);
    f_pos = vec3(model * vec4(aPos, 1.0));
    gl_Position = projection * position;
}
