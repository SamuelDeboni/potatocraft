#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 uv_in;
layout (location = 2) in vec3 normal_in;

out vec3 f_pos;
out vec2 f_uv;
out vec3 f_normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main()
{
    f_uv = uv_in;
    f_normal = normal_in;
    
    vec4 position = view * model * vec4(aPos, 1.0);
    f_pos = vec3(model * vec4(aPos, 1.0));
    gl_Position = projection * position;
}
