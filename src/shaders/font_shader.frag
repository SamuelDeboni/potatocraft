#version 330 core
out vec4 FragColor;

in vec3 f_pos;
in vec2 f_uv;

uniform vec4 tint;
uniform sampler2D tex;

vec2 normalizeUv(sampler2D t, vec2 uv, float aaf) {
    vec2 ruv = uv;
    vec2 res = textureSize(t, 0);
    ruv = ruv * res + 0.5;
    
    // tweak fractionnal value of the texture coordinate
    vec2 fl = floor(ruv);
    vec2 fr = fract(ruv);
    vec2 aa = fwidth(ruv) * aaf;
    fr = smoothstep( vec2(0.5,0.5) - aa, vec2(0.5,0.5) + aa, fr);
	ruv = (fl + fr - 0.5) / res;
    
    return ruv;
}

void main()
{
    vec2 nuv = normalizeUv(tex, f_uv, 0.5);
    FragColor = vec4(1, 1, 1, texture(tex, nuv).r) * tint;
}
