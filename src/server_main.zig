usingnamespace @import("server_index.zig");

pub fn main() !void {
    plog.logln(.server, "Initing headless server");

    const conn_args = ServerArgs{ .port = 7777 };
    try server_main(conn_args, true);
}
