const std = @import("std");

const TGAHeader = packed struct {
    id_lenth: u8,
    colour_map_type: u8,
    data_type_code: u8,
    color_map_origin: u16,
    color_map_length: u16,
    color_map_depth: u8,
    x_origin: u16,
    y_origin: u16,
    width: u16,
    height: u16,
    bits_per_pixel: u8,
    image_descriptor: u8,
};

pub const Image = struct {
    width: u32,
    height: u32,
    raw: []const u8,
};

pub const ImageArray = struct {
    width: u32 = 0,
    height: u32 = 0,
    depth: u32 = 0,
    raw: []u8 = undefined,
};

pub fn image_array_from_tga(al: *std.mem.Allocator, width: u32, height: u32, tga_data: []const []const u8) !ImageArray {
    
    var result = ImageArray{
        .width = width,
        .height = height,
        .depth = @intCast(u32, tga_data.len),
        .raw = try al.alloc(u8, width * height * tga_data.len * 4),
    };
    errdefer al.free(result.raw);
    
    var images = try al.alloc(Image, tga_data.len);
    defer al.free(images);
    
    var buf = try al.alloc(u8, width * height * 4 * tga_data.len);
    defer al.free(buf);
    var fix_al = std.heap.FixedBufferAllocator.init(buf);
    
    for (images) |*it, i| {
        it.* = try image_from_tga_data(&fix_al.allocator, tga_data[i]);
        defer fix_al.reset();
        
        if (it.width != width or it.height != height) {
            std.debug.print("{} {}\n", .{it.width, it.height});
            return error.SizeNotMatch;
        }
        
        var dat = result.raw[(i * width * height * 4)..][0..(width * height * 4)];
        for (dat) |*d, j| d.* = it.raw[j];
    }
    
    return result;
}

/// A simple function that loads a simple Runlength encoded RGBA TGA image
pub fn image_from_tga_data(al: *std.mem.Allocator, tga_data: []const u8) !Image {
    const header: *const TGAHeader = @ptrCast(*const TGAHeader, tga_data[0..@sizeOf(TGAHeader)]);
    
    // Assert that the image is Runlength encoded RGB
    if (header.data_type_code != 10) {
        return error.InvalidTGAFormat;
    }
    
    if (header.bits_per_pixel != 32) {
        return error.InvalidBitsPerPixel;
    }
    
    var data = tga_data[(@sizeOf(TGAHeader) + header.id_lenth)..][0..(tga_data.len - 26)];
    
    var result = Image{
        .width = header.width,
        .height = header.height,
        .raw = undefined,
    };
    
    var result_data = try al.alloc(u8, result.width * result.height * 4);
    errdefer al.free(result.raw);
    
    var index: usize = 0;
    var texture_index: usize = 0;
    outer_loop: while (index < data.len) {
        const pb = data[index];
        index += 1;
        const packet_len = pb & 0x7f;
        
        if ((pb & 0x80) == 0x00) { // raw packet
            var i: usize = 0;
            while (i <= packet_len) : (i += 1) {
                const alpha = data[index + 3];
                const alpha_multi = @boolToInt(alpha != 0);
                
                result_data[texture_index]     = data[index + 2] * alpha_multi;
                result_data[texture_index + 1] = data[index + 1] * alpha_multi;
                result_data[texture_index + 2] = data[index] * alpha_multi;
                result_data[texture_index + 3] = alpha;
                
                texture_index += 4;
                if (texture_index >= result_data.len - 3) break :outer_loop;
                index += 4;
            }
        } else { // rl packet
            var i: usize = 0;
            while (i <= packet_len) : (i += 1) {
                const alpha = data[index + 3];
                const alpha_multi = @boolToInt(alpha != 0);
                
                result_data[texture_index]     = data[index + 2] * alpha_multi;
                result_data[texture_index + 1] = data[index + 1] * alpha_multi;
                result_data[texture_index + 2] = data[index] * alpha_multi;
                result_data[texture_index + 3] = alpha;
                
                texture_index += 4;
                if (texture_index >= result_data.len - 3) break :outer_loop;
            }
            index += 4;
        }
    }
    
    result.raw = result_data;
    return result;
    
}
