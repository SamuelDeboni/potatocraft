pub fn cast(comptime T: type, v: anytype) T {
    const v_type = @TypeOf(v);
    const v_info = @typeInfo(v_type);
    const t_info = @typeInfo(T);
    
    switch (v_info) {
        .Int => switch (t_info) {
            .Int => return @intCast(T, v),
            .Float => return @intToFloat(T, v),
            else => @compileError("Value shoud be int\n"),
        },
        
        .Float => switch (t_info) {
            .Int => return @floatToInt(T, v),
            .Float => return @floatCast(T, v),
            else => @compileError("Value shoud be float\n"),
        },
        
        else => {
            @compileError("Unsuported type\n");
        },
    }
}