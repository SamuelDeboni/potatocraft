usingnamespace @import("index.zig");


pub const BlockTemplate = struct {
    name: []const u8,
    up_texture: u32 = 0,
    side_texture: u32 = 0,
    botton_texture: u32 = 0,
};

pub fn get_block_texture_offset(id: u32) [4]f32 {
    var result: [4]f32 = [_]f32{
        0.0,
        32.0 / 1024.0,
        32.0 / 1024.0,
        0.0,
    };
    
    const w: f32 = 32.0 / 1024.0;
    
    // HACK(Samuel): Avoid interpolation between bounderies
    const correction = 0.5 / 1024.0;
    
    result[0] += w * cast(f32, id % 32); // u0
    result[1] += w * cast(f32, id / 32); // v0
    
    result[2] += w * cast(f32, id % 32); // u1
    result[3] += w * cast(f32, id / 32); // v1
    
    result[0] += correction;
    result[1] -= correction;
    
    result[2] -= correction;
    result[3] += correction;
    
    return result;
}

pub const blocks_template = [_]BlockTemplate {
    .{
        .name = "air",
    },
    
    .{
        .name = "grass",
        .up_texture = 1,
        .side_texture = 2,
        .botton_texture = 3,
    },
    
    .{
        .name = "dirt",
        .up_texture = 3,
        .side_texture = 3,
        .botton_texture = 3,
    },
    
    .{
        .name = "stone",
        .up_texture = 4,
        .side_texture = 4,
        .botton_texture = 4,
    },
    
    .{
        .name = "gravel",
        .up_texture = 5,
        .side_texture = 5,
        .botton_texture = 5,
    },
    
    .{
        .name = "planks",
        .up_texture = 6,
        .side_texture = 6,
        .botton_texture = 6,
    },
    
    .{
        .name = "wood",
        .up_texture = 8,
        .side_texture = 7,
        .botton_texture = 8,
    },
    
    .{
        .name = "leafe",
        .up_texture = 9,
        .side_texture = 9,
        .botton_texture = 9,
    },
    
    
    .{
        .name = "potato",
        .up_texture = 0,
        .side_texture = 0,
        .botton_texture = 0,
    },
};