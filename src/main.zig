usingnamespace @import("index.zig");

var potato: Image = undefined;
var potato_tex = render.Texture{};

pub fn app_main() void {
    server.server_main(.{ .port = 7777 }, false) catch |e| {
        plog.logln(.server, "Server cannot be intialized");
    };
    
    net.connect_to_server("127.0.0.1", 7777) catch |e| {
        plog.loglnfmt(.client, "Client cannot be intialized: {}\n", .{e});
    };
    
    const tsi = image_array_from_tga
        (g_allocator, 32, 32,
         ([_][]const u8{
              @embedFile("../assets/blocks/potato.tga"),
              @embedFile("../assets/blocks/grass_top.tga"),
              @embedFile("../assets/blocks/grass_side.tga"),
              @embedFile("../assets/blocks/dirt.tga"),
              @embedFile("../assets/blocks/stone.tga"),
              @embedFile("../assets/blocks/gravel.tga"),
              @embedFile("../assets/blocks/planks.tga"),
              @embedFile("../assets/blocks/log.tga"),
              @embedFile("../assets/blocks/log_top.tga"),
              @embedFile("../assets/blocks/leaf.tga"),
          })[0..],
         ) catch @panic("AAAAA\n");
    
    render.init();
    render.clear(0.2, 0.6, 1.0);
    plt.swapBuffers();
    
    potato = image_from_tga_data(g_allocator, @embedFile("../assets/potato.tga")) catch @panic("Unable to load potato\n");
    
    potato_tex = render.upload_image(potato);
    defer render.delete_texture(potato_tex);
    
    tile_tex = render.upload_image(image_from_tga_data(std.heap.page_allocator, @embedFile("../assets/tile_set.tga")) catch @panic("Unable to load grass\n"));
    
    tile_tex_array = render.upload_image_array(tsi);
    defer render.delete_texture(tile_tex);
    
    ui_camera.transform.position.z = 10;
    var ui_ctx = dui.init(g_allocator, 80.0) catch @panic("Unable to init ui\n");
    defer dui.deinit(g_allocator, &ui_ctx);
    
    start_chunk_mesh(g_allocator) catch @panic("Allocatin failure\n");
    defer end_chunk_mesh(g_allocator);
    
    chunk_hash_table = init_chunk_hash(g_allocator, 64 * 64) catch @panic("Allocatin failure\n");
    
    //plt.disableCursor();
    
    var delta: f32 = 0.0; // Delta time between frames
    var itime: f64 = plt.getTime(); // Start time of the frame
    var ltime: f64 = 0.0;
    
    var running = true;
    while (running) {
        { // calculate delta
            ltime = itime;
            itime = plt.getTime(); // Get the initial frame time
            delta = @floatCast(f32, itime - ltime);
            if (delta > 0.1) // Prevent delta to go to large in case of lag
                delta = 0.1;
        }
        
        {
            dui.text_fmt(&ui_ctx, "{d:0.0}fps / {d:0.2}ms - ping = {d:0.2}ms\n", .{1 / delta, delta * 1000, net.last_server_ping},
                         -ui_ctx.width * 0.5 + 0.1, ui_ctx.height * 0.5 - 0.1, 1.0, 0.1);
            
            dui.text_fmt(&ui_ctx, "x = {d:0.2}, y = {d:0.2}, z = {d:0.2}", .{player.position.x, player.position.y, player.position.z},
                         -ui_ctx.width * 0.5 + 0.1, ui_ctx.height * 0.5 - 0.2, 1.0, 0.1);
        }
        
        render.clear(0.2, 0.6, 1.0);
        
        input.pollInput();
        plt.pollEvents();
        if (input.keyDown(.f11)) plt.switchFullscreen();
        
        if (plt.cursor_is_enabled and input.keyDown(.escape)) {
            plt.disableCursor();
        } else if (!plt.cursor_is_enabled and input.keyDown(.escape)) {
            plt.enableCursor();
        }
        
        do_one_frame(delta, &ui_ctx);
        draw_ui(&ui_ctx);
        
        plt.swapBuffers();
        if (plt.shouldClose()) running = false;
    }
}

var player = Player{};


fn do_one_frame(delta: f32, ui_ctx: *dui.UiContext) void {
    update_player(&player, delta, ui_ctx);
    render.camera = player.camera;
    
    { // get network event
        const held = net.using_queue_mutex.acquire();
        defer held.release();
        
        while (net.next_raw_event()) |event| {
            if (event.data[0] == @enumToInt(netdef.EventType.block_break)) {
                
                const e = @ptrCast(*const netdef.BlockBreakEvent, &event.data).*;
                
                if (get_chunk_from_hash(&chunk_hash_table, e.chunk_pos_x, e.chunk_pos_y)) |chunk| {
                    break_block(chunk, Vec3i.c(e.block_pos_x, e.block_pos_y, e.block_pos_z));
                }
            } else if (event.data[0] == @enumToInt(netdef.EventType.block_place)) {
                
                const e = @ptrCast(*const netdef.BlockPlaceEvent, &event.data).*;
                
                if (get_chunk_from_hash(&chunk_hash_table, e.chunk_pos_x, e.chunk_pos_y)) |chunk| {
                    place_block(chunk, .{.id = e.block_id}, Vec3i.c(e.block_pos_x, e.block_pos_y, e.block_pos_z));
                    std.debug.print("YAY\n", .{});
                }
            }
        }
    }
    
    { // Chunk updates
        var cp = player.camera.transform.position;
        cp.z = 0;
        
        const x_offset = @divFloor(cast(i32, cp.x - @as(f32, if (cp.x < 0) 1.0 else 0.0)), Chunk.width);
        const y_offset = @divFloor(cast(i32, cp.y - @as(f32, if (cp.y < 0) 1.0 else 0.0)), Chunk.width);
        
        for (chunk_hash_table.chunks) |*it, i| {
            if (it.state == .disabled) continue;
            if (it.state == .can_clear) {
                remove_chunk_from_hash(&chunk_hash_table, it.position.x, it.position.y);
                continue;
            }
            
            const xf = cast(f32, it.position.x) * Chunk.width;
            const yf = cast(f32, it.position.y) * Chunk.width;
            const t = Transform{
                .position = Vec3.c(xf, yf, 0),
            };
            const relative_pos = Vec3_sub(cp, Vec3_add(t.position, Vec3.c(Chunk.width / 2, Chunk.width / 2, 0)));
            const dist = cast(i32, Vec3_len(relative_pos));
            
            if (dist > (render_d + 2) * Chunk.width) {
                it.state = .can_clear;
                continue;
            }
            
            if (it.state == .need_to_update) {
                const x = it.position.x;
                const y = it.position.y;
                
                update_chunk_mesh(it, it.lod,
                                  get_chunk_from_hash(&chunk_hash_table, x + 1, y),
                                  get_chunk_from_hash(&chunk_hash_table, x - 1, y),
                                  get_chunk_from_hash(&chunk_hash_table, x, y + 1),
                                  get_chunk_from_hash(&chunk_hash_table, x, y - 1));
                
                it.state = .enabled;
            }
        }
        
        var has_generetated_a_chunk_in_this_frame = false;
        
        var yi: i32 = 0;
        while (yi < render_d) : (yi += 1) {
            var xi: i32 = 0;
            while (xi < render_d) : (xi += 1) {
                
                var y = yi + y_offset;
                const y2 = -yi + y_offset;
                
                while (true) {
                    
                    var x = xi + x_offset;
                    const x2 = -xi + x_offset;
                    
                    while (true) {
                        var c = get_or_create_chunk(&chunk_hash_table, x, y);
                        
                        const xf = cast(f32, c.position.x) * Chunk.width;
                        const yf = cast(f32, c.position.y) * Chunk.width;
                        const t = Transform{
                            .position = Vec3.c(xf, yf, 0),
                        };
                        const relative_pos = Vec3_sub(cp, Vec3_add(t.position, Vec3.c(Chunk.width / 2, Chunk.width / 2, 0)));
                        const dist = cast(i32, Vec3_len(relative_pos));
                        if (dist > render_d * Chunk.width) {
                            if (x != x2) {
                                x = x2;
                                continue;
                            }
                            break;
                        }
                        
                        if (c.state == .need_to_gen) {
                            if (!has_generetated_a_chunk_in_this_frame) {
                                gen_chunk(c);
                                net.send_chunk_request_event(c.position);
                                c.mesh = gen_chunk_mesh(c, 1, get_chunk_from_hash(&chunk_hash_table, x + 1, y),
                                                        get_chunk_from_hash(&chunk_hash_table, x - 1, y),
                                                        get_chunk_from_hash(&chunk_hash_table, x, y + 1),
                                                        get_chunk_from_hash(&chunk_hash_table, x, y - 1));
                                c.state = .need_to_update;
                                has_generetated_a_chunk_in_this_frame = true;
                            } else {
                                if (x != x2) {
                                    x = x2;
                                    continue;
                                }
                                break;
                            }
                        }
                        
                        
                        if (c.lod == 1 and dist > lod_d) {
                            c.lod = 2;
                            c.state = .need_to_update;
                        } else if (c.lod == 2 and dist > lod_d * 4) {
                            c.lod = 4;
                            c.state = .need_to_update;
                        } else if (c.lod == 4 and dist < lod_d * 4) {
                            c.lod = 2;
                            c.state = .need_to_update;
                        } else if (c.lod == 2 and dist < lod_d) {
                            c.lod = 1;
                            c.state = .need_to_update;
                        }
                        
                        render.draw_chunk(c.mesh, t, tile_tex_array);
                        
                        
                        if (x != x2) {
                            x = x2;
                            continue;
                        }
                        break;
                    }
                    
                    if (y != y2) {
                        y = y2;
                        continue;
                    }
                    break;
                }
            }
        }
    }
}


var lod_d: i32 = 128;
var render_d: i32 = 5;

var panel_x: f32 = -1.5;
var panel_y: f32 = 0.5;

fn draw_ui(ui_ctx: *dui.UiContext) void {
    render.camera = ui_camera;
    
    dui.start(ui_ctx);
    dui.rectWithBorder(-0.01, -0.01, 0.02, 0.02, dui.UiColor.c(1, 1, 1, 1), dui.UiColor.c(0, 0, 0, 1));
    if (plt.cursor_is_enabled) {
        var box_info = dui.lStartPanel(ui_ctx, "Menu", panel_x, panel_y, 0.6, 0.1, 0.01);
        const layout = &box_info.layout;
        
        defer {
            dui.lEndPanel(box_info);
            panel_x = box_info.x;
            panel_y = box_info.y;
        }
        
        dui.lSliderI32(layout, "LOD distance", &lod_d, 0, 512);
        dui.lSliderI32(layout, "Render distance", &render_d, 0, 20);
    }
    
    dui.end(ui_ctx);
}
