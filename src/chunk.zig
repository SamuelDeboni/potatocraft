usingnamespace @import("index.zig");

pub const ChunkVert = packed struct {
    x: u8 = 0,
    y: u8 = 0,
    z: u16 = 0,
    u: u8 = 0,
    v: u8 = 0,
    nx: i8 = 0,
    ny: i8 = 0,
    nz: i8 = 0,
    tex: u16 = 0,
};

pub const Blocks = struct {
    owner: u32 = 0,
    data: [Chunk.ssize * Chunk.height]Block = [_]Block{.{}} ** (Chunk.ssize * Chunk.height),
};

pub const ChunkHashTable = struct {
    capacity: u32 = 8196,
    chunks: []Chunk,
    
    blocks_arena: []Blocks,
    block_arena_count: u32 = 0,
};

pub fn block_from_world_pos(position: Vec3) Block {
    var result = Block{};
    
    const block_pos = world_to_block_position(position);
    const chunk_pos = chunk_position_from_block_position(block_pos);
    const chunk_coord = block_position_relative_to_chunk(block_pos);
    
    
    if (get_chunk_from_hash(&chunk_hash_table, chunk_pos.x, chunk_pos.y)) |c| {
        const cx = cast(u32, chunk_coord.x);
        const cy = cast(u32, chunk_coord.y);
        const cz = cast(u32, chunk_coord.z);
        result = c.blocks.data[Chunk.block_i(cx, cy, cz)];
    }
    
    return result;
}

pub fn world_to_block_position(position: Vec3) callconv(.Inline) Vec3i {
    const result = Vec3i {
        .x = cast(i32, position.x - @as(f32, if (position.x < 0) 1.0 else 0.0)),
        .y = cast(i32, position.y - @as(f32, if (position.y < 0) 1.0 else 0.0)),
        .z = cast(i32, position.z),
    };
    return result;
}

pub fn block_position_relative_to_chunk(position: Vec3i) callconv(.Inline) Vec3i {
    var xi = @rem(position.x, Chunk.width);
    if (xi < 0) xi += Chunk.width;
    
    var yi = @rem(position.y, Chunk.width);
    if (yi < 0) yi += Chunk.width;
    
    const result = Vec3i {
        .x = xi,
        .y = yi,
        .z = std.math.clamp(position.z, 0, Chunk.height - 1),
    };
    
    return result;
}

pub fn chunk_position_from_block_position(position: Vec3i) callconv(.Inline) Vec3i {
    const result = Vec3i {
        .x = @divFloor(position.x, Chunk.width),
        .y = @divFloor(position.y, Chunk.width),
        .z = 0,
    };
    return result;
}


// TODO(Samuel): Better hash function
pub fn chunk_hash(x: i32, y: i32, capacity: u64) u32 {
    const result = cast(u32, @mod(x *% 487 +% y *% 331, cast(i64, capacity)));
    return result;
}

pub fn get_chunk_from_hash(hash_table: *ChunkHashTable, x: i32, y: i32) ?*Chunk {
    var i = chunk_hash(x, y, hash_table.capacity);
    
    var iter: u32 = 0;
    while (iter < hash_table.capacity) : (iter += 1) {
        var c: *Chunk = &hash_table.chunks[i];
        
        if (c.state == .disabled) {
            continue;
        }
        
        if (c.position.x == x and c.position.y == y) {
            return c;
        }
        
        std.debug.print("yay\n", .{});
        i = (i + 1) % hash_table.capacity;
    }
    
    return null;
}

pub fn get_or_create_chunk(hash_table: *ChunkHashTable, x: i32, y: i32) *Chunk {
    var i = chunk_hash(x, y, hash_table.capacity);
    
    var iter: u32 = 0;
    while (iter < hash_table.capacity) : (iter += 1) {
        var c: *Chunk = &hash_table.chunks[i];
        
        if (c.state == .disabled) {
            c.position.x = x;
            c.position.y = y;
            c.state = .need_to_gen;
            
            c.blocks = &hash_table.blocks_arena[hash_table.block_arena_count];
            c.blocks.owner = i;
            hash_table.block_arena_count += 1;
            
            return c;
        }
        
        if (c.position.x == x and c.position.y == y) {
            return c;
        }
        
        std.debug.print("yay\n", .{});
        i = (i + 1) % hash_table.capacity;
    }
    
    // TODO(Samuel): better error handling
    @panic("Hash full\n");
}

pub fn remove_chunk_from_hash(hash_table: *ChunkHashTable, x: i32, y: i32) void {
    var i = chunk_hash(x, y, hash_table.capacity);
    
    while (i < hash_table.capacity) {
        var c: *Chunk = &hash_table.chunks[i];
        
        if (c.state != .disabled and c.position.x == x and c.position.y == y) {
            c.state = .disabled;
            
            render.delete_mesh(&c.mesh);
            
            c.blocks.* = hash_table.blocks_arena[hash_table.block_arena_count - 1];
            hash_table.chunks[c.blocks.owner].blocks = c.blocks;
            
            hash_table.block_arena_count -= 1;
            
            break;
        }
        
        std.debug.print("yay\n", .{});
        i += 1;
    }
}

pub fn init_chunk_hash(al: *Allocator, capacity: u32) !ChunkHashTable {
    var ch = ChunkHashTable {
        .chunks = try al.alloc(Chunk, capacity),
        .blocks_arena = try al.alloc(Blocks, capacity),
        .capacity = capacity,
    };
    
    for (ch.chunks) |*it| it.* = .{.blocks = undefined};
    
    var y: i32 = -4;
    while (y < 4) : (y += 1) {
        var x: i32 = -4;
        while (x < 4) : (x += 1) {
            const c = get_or_create_chunk(&ch, x, y);
            gen_chunk(c);
        }
    }
    
    y  = -4;
    while (y < 4) : (y += 1) {
        var x: i32 = -4;
        while (x < 4) : (x += 1) {
            const chunk = get_chunk_from_hash(&ch, x, y);
            if (chunk) |c| {
                c.mesh = gen_chunk_mesh(c, 1,
                                        get_chunk_from_hash(&ch, x + 1, y),
                                        get_chunk_from_hash(&ch, x - 1, y),
                                        get_chunk_from_hash(&ch, x, y + 1),
                                        get_chunk_from_hash(&ch, x, y - 1));
            }
        }
    }
    
    return ch;
}

pub const Block = struct {
    id: u16 = 0,
};

pub const ChunkState = enum {
    disabled,
    enabled,
    need_to_gen,
    need_to_update,
    can_clear,
};

pub const Chunk = struct {
    state: ChunkState = .disabled,
    
    blocks: *Blocks,
    
    position: Vec3i = .{},
    mesh: render.Mesh = .{},
    lod: u32 = 1,
    
    pub const height = 512;
    pub const width = 32;
    pub const ssize = width * width;
    
    pub fn block_i(x: u32, y: u32, z: u32) callconv(.Inline) u32 {
        return x + y * width + z * ssize;
    }
    
    pub fn get_block(c: *const Chunk, _x: u32, _y: u32, _z: u32, lod: u32) Block {
        var result = c.blocks.data[ block_i(_x, _y, _z) ];
        if (lod == 1) return result;
        
        var zi: u32 = 0;
        while (zi <= lod) : (zi += 1) {
            const z = std.math.clamp(zi + _z, 0, height - lod);
            
            var yi: u32 = 0;
            while (yi <= lod) : (yi += 1) {
                const y = std.math.clamp(yi + _y, 0, width - lod);
                
                var xi: u32 = 0;
                while (xi <= lod) : (xi += 1) {
                    const x = std.math.clamp(xi + _x, 0, width - lod);
                    
                    if (c.blocks.data[block_i(x, y, z)].id == 0) {
                        result = c.blocks.data[block_i(x, y, z)];
                        //break: outer;
                    }
                }
            }
        }
        
        return result;
    }
};

pub fn gen_chunk(chunk: *Chunk) void {
    var noise: [Chunk.ssize]f32 = [_]f32 {0.0} ** Chunk.ssize;
    
    var y: u32 = 0;
    while (y < Chunk.width) : (y += 1) {
        var x: u32 = 0;
        while (x < Chunk.width) : (x += 1) {
            const i = x + y * Chunk.width;
            
            const bx = cast(f32, x) + cast(f32, chunk.position.x * Chunk.width);
            const by = cast(f32, y) + cast(f32, chunk.position.y * Chunk.width);
            
            var moutain_map = perlin_2d(bx * 0.004, by * 0.002) * 2;
            moutain_map = std.math.clamp(moutain_map, 0.0, 1.0);
            var moutain_noise = perlin_2d(bx * 0.015, by * 0.015) * 50 + 100;
            
            var h1 = perlin_2d(bx * 0.01, by * 0.01) * 30;
            var h2 = perlin_2d(bx * 0.02, by * 0.02) * 10;
            var h3 = perlin_2d(bx * 0.09, by * 0.09) * 5;
            var h = 128 + h2 + h1 + (h3 + moutain_noise) * moutain_map;
            
            
            noise[i] = h;
        }
    }
    
    
    var z: u32 = 0;
    while (z < Chunk.height) : (z += 1) {
        y = 0;
        while (y < Chunk.width) : (y += 1) {
            var x: u32 = 0;
            while (x < Chunk.width) : (x += 1) {
                const i = x + y * Chunk.width + z * Chunk.ssize;
                const j = x + y * Chunk.width;
                
                var nh = cast(u32, noise[j]);
                
                if (z <= nh) {
                    if (z > 160) {
                        if (z < nh) {
                            chunk.blocks.data[i].id = 3;
                        } else {
                            chunk.blocks.data[i].id = 4;
                        }
                    } else if (z > 140) {
                        if (z < nh) {
                            chunk.blocks.data[i].id = 3;
                        } else {
                            chunk.blocks.data[i].id = 2;
                        }
                    } else {
                        if (z < nh - 5) {
                            chunk.blocks.data[i].id = 3;
                        } else if (z < nh) {
                            chunk.blocks.data[i].id = 2;
                        } else {
                            chunk.blocks.data[i].id = 1;
                        }
                    }
                    
                } else {
                    chunk.blocks.data[i].id = 0;
                }
            }
        }
    }
}

const Dir = enum {
    up,
    down,
    north,
    south,
    east,
    west,
};

fn add_face(vert: []ChunkVert, vc: *u32, ind: []u32, ic: *u32, dir: Dir, _x: u32, _y: u32, _z: u32, _lod: u32, id: u32) void {
    const vert0 = vc.*;
    
    const x = cast(u8, _x);
    const y = cast(u8, _y);
    const z = cast(u16, _z);
    const lod = cast(u8, _lod);
    
    const b = blocks_template[id];
    var tex_pos: u16 = cast(u16, b.side_texture);
    if (dir == .up) {
        tex_pos = cast(u16, b.up_texture);
    } else if (dir == .down) {
        tex_pos = cast(u16, b.botton_texture);
    }
    
    const u = cast(u8, tex_pos % 32);
    const v = cast(u8, tex_pos / 32);
    
    if (dir == .up) {
        vert[vc.*].x = x;
        vert[vc.*].y = y;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0.0;
        vert[vc.*].ny = 0.0;
        vert[vc.*].nz = 127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
    } else if (dir == .down) {
        vert[vc.*].x = x;
        vert[vc.*].y = y;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = -127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = -127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = -127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = -127;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
    } else if (dir == .north) {
        vert[vc.*].x = x;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = 127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
    } else if (dir == .south) {
        vert[vc.*].x = x;
        vert[vc.*].y = y;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = -127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = -127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = -127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 0;
        vert[vc.*].ny = -127;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
    } else if (dir == .east) {
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = 127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x + lod;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = 127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
    } else if (dir == .west) {
        vert[vc.*].x = x;
        vert[vc.*].y = y;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = -127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z + lod;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v;
        
        vert[vc.*].nx = -127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = -127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
        
        vert[vc.*].x = x;
        vert[vc.*].y = y + lod;
        vert[vc.*].z = z;
        
        vert[vc.*].u = u + 1;
        vert[vc.*].v = v + 1;
        
        vert[vc.*].nx = -127;
        vert[vc.*].ny = 0;
        vert[vc.*].nz = 0;
        
        vert[vc.*].tex = tex_pos;
        
        vc.* += 1;
    }
    
    if (dir == .east or dir == .down or dir == .south) {
        ind[ic.*] = vert0;
        ind[ic.* + 1] = vert0 + 3;
        ind[ic.* + 2] = vert0 + 2;
        
        ind[ic.* + 3] = vert0;
        ind[ic.* + 4] = vert0 + 1;
        ind[ic.* + 5] = vert0 + 3;
        ic.* += 6;
    } else {
        ind[ic.*] = vert0;
        ind[ic.* + 1] = vert0 + 2;
        ind[ic.* + 2] = vert0 + 3;
        
        ind[ic.* + 3] = vert0;
        ind[ic.* + 4] = vert0 + 3;
        ind[ic.* + 5] = vert0 + 1;
        ic.* += 6;
    }
}


//var v_tmp: []f32 = undefined;
var v_tmp: []ChunkVert = undefined;
var i_tmp: []u32 = undefined;

pub fn start_chunk_mesh(al: *Allocator) !void {
    v_tmp = try al.alloc(ChunkVert, Chunk.ssize * Chunk.height * 6 * 4);
    i_tmp = try al.alloc(u32, Chunk.ssize * Chunk.height * 6 * 6);
}

pub fn end_chunk_mesh(al: *Allocator) void {
    al.free(v_tmp);
    al.free(i_tmp);
}

fn gen_chunk_mesh_inner(chunk: *const Chunk, _vc: *u32, _ic: *u32, lod: u32,
                        chunk_e: ?*const Chunk, chunk_w: ?*const Chunk,
                        chunk_n: ?*const Chunk, chunk_s: ?*const Chunk) void
{
    var vc = _vc.*;
    var ic = _ic.*;
    
    var z: u32 = 0;
    while (z < Chunk.height) : (z += lod) {
        
        var y: u32 = 0;
        while (y < Chunk.width) : (y += lod) {
            
            var x: u32 = 0;
            while (x < Chunk.width) : (x += lod) {
                if (chunk.blocks.data[Chunk.block_i(x, y, z)].id == 0) continue;
                const id = chunk.blocks.data[Chunk.block_i(x, y, z)].id;
                
                const b_up: u32 = if (z == Chunk.height - lod) 0
                    else chunk.blocks.data[Chunk.block_i(x, y, z + lod)].id;
                const b_down: u32 = if (z < lod) 0
                    else chunk.blocks.data[Chunk.block_i(x, y, z - lod)].id;
                
                if (b_up == 0) {
                    if (lod == 4) {
                        var _id = id;
                        var id2 = chunk.blocks.data[Chunk.block_i(x, y, z + 1)].id;
                        var id3 = chunk.blocks.data[Chunk.block_i(x, y, z + 2)].id;
                        var id4 = chunk.blocks.data[Chunk.block_i(x, y, z + 3)].id;
                        if (id2 != 0) _id = id2;
                        if (id3 != 0) _id = id3;
                        if (id4 != 0) _id = id4;
                        add_face(v_tmp, &vc, i_tmp, &ic, .up, x, y, z, lod, _id);
                    } else if (lod == 2) {
                        var id2 = chunk.blocks.data[Chunk.block_i(x, y, z + 1)].id;
                        var _id = if (id2 != 0) id2 else id;
                        add_face(v_tmp, &vc, i_tmp, &ic, .up, x, y, z, lod, _id);
                    } else {
                        add_face(v_tmp, &vc, i_tmp, &ic, .up, x, y, z, lod, id);
                    }
                }
                
                if (b_down == 0)
                    add_face(v_tmp, &vc, i_tmp, &ic, .down, x, y, z, lod, id);
                
                var b_east: u32 = 0;
                if (x >= Chunk.width - lod) {
                    if (chunk_e) |ce| {
                        b_east = ce.get_block(0, y, z, lod).id;
                    }
                } else {
                    b_east = chunk.blocks.data[Chunk.block_i(x + lod, y, z)].id;
                }
                
                var b_west: u32 = 0;
                if (x == 0) {
                    if (chunk_w) |cw| {
                        b_west = cw.get_block(Chunk.width - lod, y, z, lod).id;
                    }
                } else {
                    b_west = chunk.blocks.data[Chunk.block_i(x - lod, y, z)].id;
                }
                
                var b_north: u32 = 0;
                if (y >= Chunk.width - lod) {
                    if (chunk_n) |cn| {
                        b_north = cn.get_block(x, 0, z, lod).id;
                    }
                } else {
                    b_north = chunk.blocks.data[Chunk.block_i(x, y + lod, z)].id;
                }
                
                var b_south: u32 = 0;
                if (y == 0) {
                    if (chunk_s) |cs| {
                        b_south = cs.get_block(x, Chunk.width - lod, z, lod).id;
                    }
                } else {
                    b_south = chunk.blocks.data[Chunk.block_i(x, y - lod, z)].id;
                }
                
                if (b_west == 0)
                    add_face(v_tmp, &vc, i_tmp, &ic, .west, x, y, z, lod, id);
                if (b_east == 0)
                    add_face(v_tmp, &vc, i_tmp, &ic, .east, x, y, z, lod, id);
                if (b_south == 0)
                    add_face(v_tmp, &vc, i_tmp, &ic, .south, x, y, z, lod, id);
                if (b_north == 0)
                    add_face(v_tmp, &vc, i_tmp, &ic, .north, x, y, z, lod, id);
                
            }
        }
    }
    
    _vc.* = vc;
    _ic.* = ic;
}

pub fn gen_chunk_mesh(chunk: *const Chunk, lod: u32, chunk_e: ?*const Chunk, chunk_w: ?*const Chunk, chunk_n: ?*const Chunk, chunk_s: ?*const Chunk) render.Mesh
{
    var vc: u32 = 0;
    var ic: u32 = 0;
    
    gen_chunk_mesh_inner(chunk, &vc, &ic, lod, chunk_e, chunk_w, chunk_n, chunk_s);
    
    const vertex = v_tmp[0..vc];
    const index  = i_tmp[0..ic];
    
    return render.create_mesh_chunk(vertex, index);
}


pub fn update_chunk_mesh(chunk: *Chunk, lod: u32, chunk_e: ?*const Chunk, chunk_w: ?*const Chunk, chunk_n: ?*const Chunk, chunk_s: ?*const Chunk) void
{
    var vc: u32 = 0;
    var ic: u32 = 0;
    
    gen_chunk_mesh_inner(chunk, &vc, &ic, lod, chunk_e, chunk_w, chunk_n, chunk_s);
    
    const vertex = v_tmp[0..vc];
    const index  = i_tmp[0..ic];
    
    render.update_mesh_chunk(&chunk.mesh, vertex, index);
}