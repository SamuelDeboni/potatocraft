usingnamespace @import("server_index.zig");

pub const ServerArgs = struct {
    port: u16 = 7777,
};

var running = true;
pub var inited = false;
pub var headless = true;

var tmp_blocks = Blocks{};
var tmp_chunk = Chunk{
    .blocks = &tmp_blocks,
};

fn server_loop(args: ServerArgs) void {
    plog.logln(.server, "Initing enet...");
    assert(enet.enet_initialize() == 0);
    
    const address = enet.ENetAddress {
        .host = enet.ENET_HOST_ANY,
        .port = args.port,
    };
    
    const server = enet.enet_host_create(&address, 32, 1, 0, 0);
    assert(server != null);
    
    plog.logln(.server, "Enet inited with success");
    
    inited = true;
    
    while (running) {
        var event: enet.ENetEvent = undefined;
        while (enet.enet_host_service(server, &event, 100) > 0) {
            switch (event.type) {
                .ENET_EVENT_TYPE_CONNECT => plog.logln(.server, "connection event"),
                
                .ENET_EVENT_TYPE_RECEIVE => {
                    const data = event.packet.*.data[0..event.packet.*.dataLength];
                    //plog.loglnfmt(.server, "packet: {s}", .{data});
                    
                    switch (data[0]) {
                        @enumToInt(netdef.EventType.ping) =>  {
                            var packet_data = [_]u8{0, 'y', 'o', 'o'};
                            packet_data[0] = @enumToInt(netdef.EventType.pong);
                            
                            const packet = enet.enet_packet_create(packet_data[0..], 4, enet.ENET_PACKET_FLAG_RELIABLE);
                            _ = enet.enet_peer_send(event.peer, 0, packet);
                        },
                        
                        @enumToInt(netdef.EventType.chunk_request) => {
                            const e = @ptrCast(*netdef.ChunkRequestEvent, event.packet.*.data);
                            
                            plog.loglnfmt(.server, "Chunk Request on pos {} {}\n", .{e.chunk_pos_x, e.chunk_pos_y});
                            tmp_chunk.position.x = e.chunk_pos_x;
                            tmp_chunk.position.y = e.chunk_pos_y;
                        },
                        
                        else => {
                            plog.logln(.server, "Broadcasting event");
                            _ = enet.enet_host_broadcast(server, 0, event.packet);
                        },
                    }
                },
                
                .ENET_EVENT_TYPE_DISCONNECT => std.debug.print("disconnect event\n", .{}),
                else => std.debug.print("Unknow event type\n", .{}),
            }
        }
    }
}

pub fn server_stop() void {
    running = false;
}


pub fn server_main(args: ServerArgs, _headless: bool) !void {
    headless = _headless;
    if (headless) {
        server_loop(args);
    } else {
        const server_thread = try std.Thread.spawn(server_loop, args);
    }
}
