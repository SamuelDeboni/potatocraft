usingnamespace @import("index.zig");
const KeyState = plt.KeyState;
const Key = plt.Key;

var prev_mouse_pos = [2]f32{0.0, 0.0};
pub var mouse_pos = [2]f32{0.0, 0.0};
pub var mouse_acceleration = [2]f32{0.0, 0.0};
pub var mouse_scroll = [2]f32{0.0, 0.0};

pub const mouse_buttons = enum(u8) {
    left = (1 << 0),
    right = (1 << 1),
    middle = (1 << 2),
};

var key_previus_state = [_]KeyState{.release} ** plt.keymap.len;

pub var mouse_pressed: u8 = 0;
pub var mouse_up: u8 = 0;
pub var mouse_down: u8 = 0;

pub var character_input_queue: [256]u32 = undefined;
pub var character_input_queue_size: usize = 0;

pub var input_is_blocked = false;

pub fn getCharInputSlice() callconv(.Inline) []const u32 {
    return character_input_queue[0..character_input_queue_size];
}

pub fn pollInput() void {
    character_input_queue_size = 0;
    mouse_down = 0;
    mouse_up = 0;
    mouse_scroll = [_]f32 {0.0, 0.0};
    
    
    for (key_previus_state) |*it, i| {
        it.* = plt.getKeyState(@intToEnum(Key, i));
    }
    
    prev_mouse_pos[0] = mouse_pos[0];
    prev_mouse_pos[1] = mouse_pos[1];
    
    mouse_pos = plt.mousePos();
    
    mouse_acceleration[0] = mouse_pos[0] - prev_mouse_pos[0];
    mouse_acceleration[1] = mouse_pos[1] - prev_mouse_pos[1];
}

pub fn keyDown(key: Key) bool {
    const kp = @intCast(usize, @enumToInt(key));
    const previus = key_previus_state[kp];
    const state = plt.getKeyState(key);
    
    var result = false;
    
    if (previus == .release and state == .press) {
        result = !input_is_blocked;
    }
    
    return result;
}

pub fn keyDownNoBlock(key: Key) bool {
    const kp = @intCast(usize, @enumToInt(key));
    const previus = key_previus_state[kp];
    const state = plt.getKeyState(key);
    
    var result = false;
    
    if (previus == .release and state == .press) {
        result = true;
    }
    
    return result;
}

pub fn keyUp(key: Key) bool {
    const kp = @intCast(usize, @enumToInt(key));
    const previus = key_previus_state[kp];
    const state = plt.getKeyState(key);
    
    var result = false;
    
    if (previus == .press and state == .release) {
        result = !input_is_blocked;
    }
    
    return result;
}

pub fn keyPressed(key: Key) bool {
    const result = plt.getKeyState(key) == .press and !input_is_blocked;
    return result;
}

pub fn pressedAsInt(key: Key) i32 {
    return @intCast(i32, @boolToInt(keyPressed(key)));
}

pub fn pressedAsFloat(key: Key) f32 {
    return @intToFloat(f32, @boolToInt(keyPressed(key)));
}

pub fn mouseDown(button: mouse_buttons) bool {
    return (@enumToInt(button) & mouse_down) == @enumToInt(button);
}

pub fn mouseUp(button: mouse_buttons) bool {
    return (@enumToInt(button) & mouse_up) == @enumToInt(button);
}

pub fn mousePressed(button: mouse_buttons) bool {
    return (@enumToInt(button) & mouse_pressed) == @enumToInt(button);
}
