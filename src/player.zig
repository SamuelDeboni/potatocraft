usingnamespace @import("index.zig");

pub const Player = struct {
    position: Vec3 = .{.z = 142},
    velocity: Vec3 = .{},
    head_dir: Vec3 = .{},
    selected_block: u16 = 1,
    camera: render.Camera = .{},
    on_floor: bool = false,
};

pub fn break_block(chunk: *Chunk, pos: Vec3i) void {
    chunk.blocks.data[Chunk.block_i(cast(u32, pos.x), cast(u32, pos.y), cast(u32, pos.z))] = .{};
    chunk.state = .need_to_update;
    
    const r_pos = block_position_relative_to_chunk(pos);
    if (r_pos.x == 0) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x - 1, chunk.position.y)) |c|
            c.state = .need_to_update;
    } else if (r_pos.x == Chunk.width - 1) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x + 1, chunk.position.y)) |c|
            c.state = .need_to_update;
    }
    
    if (r_pos.y == 0) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x, chunk.position.y - 1)) |c|
            c.state = .need_to_update;
    } else if (r_pos.y == Chunk.width - 1) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x, chunk.position.y + 1)) |c|
            c.state = .need_to_update;
    }
    
}

pub fn place_block(chunk: *Chunk, block: Block, pos: Vec3i) void {
    chunk.blocks.data[Chunk.block_i(cast(u32, pos.x), cast(u32, pos.y), cast(u32, pos.z))] = block;
    
    chunk.state = .need_to_update;
    
    const r_pos = block_position_relative_to_chunk(pos);
    if (r_pos.x == 0) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x - 1, chunk.position.y)) |c|
            c.state = .need_to_update;
    } else if (r_pos.x == Chunk.width - 1) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x + 1, chunk.position.y)) |c|
            c.state = .need_to_update;
    }
    
    if (r_pos.y == 0) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x, chunk.position.y - 1)) |c|
            c.state = .need_to_update;
    } else if (r_pos.y == Chunk.width - 1) {
        if (get_chunk_from_hash(&chunk_hash_table, chunk.position.x, chunk.position.y + 1)) |c|
            c.state = .need_to_update;
    }
}

pub fn update_player(player: *Player, delta: f32, ui_ctx: *dui.UiContext) void {
    
    var forward_camera: Rotor3 = .{};
    { // Rotation
        
        const ry: f32 = if (!plt.cursor_is_enabled) input.mouse_acceleration[0] * 0.2 else 0.0;
        const rx: f32 = if (!plt.cursor_is_enabled) input.mouse_acceleration[1] * -0.2 else 0.0;
        
        // Rotation
        player.head_dir.x += rx * delta;
        player.head_dir.y += ry * delta;
        player.head_dir.x = std.math.clamp(player.head_dir.x, -3.14 * 0.5, 3.14 * 0.5);
        
        const rot1 = Rotor3.cp(Bivec3.c(0, 0, 1), player.head_dir.x);
        const rot2 = Rotor3.cp(Bivec3.c(0, 1, 0), player.head_dir.y);
        forward_camera = Rotor3_mul(Rotor3.cp(Bivec3.c(0, 0, 1), 3.1415926535 * 0.5), rot2);
        player.camera.transform.rotation = Rotor3_mul(forward_camera, rot1);
    }
    
    {// Movment
        const forward = input.pressedAsFloat(.w)     - input.pressedAsFloat(.s);
        const right   = input.pressedAsFloat(.d)     - input.pressedAsFloat(.a);
        const up      = input.pressedAsFloat(.space) - input.pressedAsFloat(.left_shift);
        const player_speed: f32 = if (input.keyPressed(.left_control)) 6.0 else 4.0;
        const acceleration: f32 = if (player.on_floor) 30.0 else 10.0;
        
        const move_forward = Vec3_rotate(Vec3.c(0, 0, -forward), forward_camera);
        const move_right   = Vec3_rotate(Vec3.c(right, 0, 0), player.camera.transform.rotation);
        const move = Vec3_normalize(Vec3_add(move_forward, move_right));
        
        player.velocity = Vec3_add(player.velocity, Vec3_mul_F(move, acceleration * delta));
        
        if (forward == 0 and right == 0) {
            const dec: f32 = if (player.on_floor) 20.0 else 5.0;
            player.velocity.x -= dec * delta * clamp(player.velocity.x, -1.0, 1.0);
            player.velocity.y -= dec * delta * clamp(player.velocity.y, -1.0, 1.0);
            
            if (player.velocity.x < 0.1 and player.velocity.x > -0.1) player.velocity.x = 0.0;
            if (player.velocity.y < 0.1 and player.velocity.y > -0.1) player.velocity.y = 0.0;
        }
        
        var clamped_vel = player.velocity;
        clamped_vel.z = 0;
        clamped_vel = Vec3_clamp_len(clamped_vel, 0.0, player_speed);
        player.velocity.x = clamped_vel.x;
        player.velocity.y = clamped_vel.y;
        
        player.velocity.z -= 20 * delta;
        player.velocity.z = std.math.clamp(player.velocity.z, -20.0, 50.0);
        
        var new_position = player.position;
        new_position = Vec3_add(player.position, Vec3_mul_F(player.velocity, delta));
        
        //new_position = Vec3_add(move_forward, new_position);
        //new_position = Vec3_add(camera_right, new_position);
        //new_position.z += player.velocity.z * delta;
        
        var can_jump = false;
        {
            const pz = Vec3.c(player.position.x, player.position.y, new_position.z);
            const cz0 = block_from_world_pos(Vec3_add(pz, Vec3.c(0.24,  0.24, 0.0))).id != 0 or
                block_from_world_pos(Vec3_add(pz, Vec3.c(0.24, -0.24, 0.0))).id != 0 or
                block_from_world_pos(Vec3_add(pz, Vec3.c(-0.24, 0.24, 0.0))).id != 0 or
                block_from_world_pos(Vec3_add(pz, Vec3.c(-0.24,-0.24, 0.0))).id != 0;
            
            const cz1 = (block_from_world_pos(Vec3_add(pz, Vec3.c(0.24,  0.24, 1.8))).id != 0 or
                         block_from_world_pos(Vec3_add(pz, Vec3.c(0.24, -0.24, 1.8))).id != 0 or
                         block_from_world_pos(Vec3_add(pz, Vec3.c(-0.24, 0.24, 1.8))).id != 0 or
                         block_from_world_pos(Vec3_add(pz, Vec3.c(-0.24,-0.24, 1.8))).id != 0) and
                player.velocity.z > 0.0;
            
            if (cz0 or cz1) {
                new_position.z = player.position.z;
                player.velocity.z = 0;
                
                if (cz0) {
                    can_jump = true;
                }
            }
            
            player.on_floor = can_jump;
            
            const px = Vec3.c(new_position.x, player.position.y, player.position.z);
            const py = Vec3.c(player.position.x, new_position.y, player.position.z);
            
            const cx0 =
                block_from_world_pos(Vec3_add(Vec3.c(0.25,  0.25, 0.0), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(0.25, -0.25, 0.0), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25, 0.25, 0.0), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25,-0.25, 0.0), px)).id != 0;
            
            const cy0 =
                block_from_world_pos(Vec3_add(Vec3.c(0.25, -0.25, 0.0), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(0.25,  0.25, 0.0), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25,-0.25, 0.0), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25, 0.25, 0.0), py)).id != 0;
            
            const cx1 =
                block_from_world_pos(Vec3_add(Vec3.c(0.25,  0.25, 1.0), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(0.25, -0.25, 1.0), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25, 0.25, 1.0), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25,-0.25, 1.0), px)).id != 0;
            
            const cy1 =
                block_from_world_pos(Vec3_add(Vec3.c(0.25, -0.25, 1.0), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(0.25,  0.25, 1.0), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25,-0.25, 1.0), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25, 0.25, 1.0), py)).id != 0;
            
            const cx2 =
                block_from_world_pos(Vec3_add(Vec3.c(0.25,  0.25, 1.8), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(0.25, -0.25, 1.8), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25, 0.25, 1.8), px)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25,-0.25, 1.8), px)).id != 0;
            
            const cy2 =
                block_from_world_pos(Vec3_add(Vec3.c(0.25, -0.25, 1.8), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(0.25,  0.25, 1.8), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25,-0.25, 1.8), py)).id != 0 or
                block_from_world_pos(Vec3_add(Vec3.c(-0.25, 0.25, 1.8), py)).id != 0;
            
            if (cx0 or cx1 or cx2) {
                new_position.x = player.position.x;
                if (!cx1 and !cx2) {
                    new_position.z += delta * 10;
                }
            }
            
            if (cy0 or cy1 or cy2) {
                new_position.y = player.position.y;
                if (!cy1 and !cx2) {
                    new_position.z += delta * 10;
                }
            }
        }
        
        if (can_jump and input.keyPressed(.space)) player.velocity.z = 8;
        
        player.position = new_position;
        player.camera.transform.position = Vec3_add(player.position, Vec3.c(0, 0, 1.6));
    }
    
    
    { // Select block
        if (input.keyDown(._1)) {
            player.selected_block = 1;
        } else if (input.keyDown(._2)) {
            player.selected_block = 2;
        } else if (input.keyDown(._3)) {
            player.selected_block = 3;
        } else if (input.keyDown(._4)) {
            player.selected_block = 4;
        } else if (input.keyDown(._5)) {
            player.selected_block = 5;
        } else if (input.keyDown(._6)) {
            player.selected_block = 6;
        } else if (input.keyDown(._7)) {
            player.selected_block = 7;
        } else if (input.keyDown(._8)) {
            player.selected_block = 8;
        }
        
        if (input.mouse_scroll[1] < 0) {
            player.selected_block += 1;
            if (player.selected_block >= blocks_template.len)
                player.selected_block = 1;
        } else if (input.mouse_scroll[1] > 0) {
            if (player.selected_block == 1) {
                player.selected_block = blocks_template.len - 1;
            } else {
                player.selected_block -= 1;
            }
        }
        
        {
            render.camera = ui_camera;
            
            const s: f32 = 0.25;
            var x: f32 = -s * 4;
            var y: f32 = -ui_ctx.height * 0.5 + 0.15;
            
            var bg_color = dui.UiColor.c(0.2, 0.2, 0.2, 1);
            var fg_color = dui.UiColor.c(0.5, 0.5, 0.5, 1);
            
            var i: u32 = 1;
            while (i < 9) : (i += 1) {
                if (i == player.selected_block) {
                    fg_color = dui.UiColor.c(1, 1, 1, 1);
                } else {
                    fg_color = dui.UiColor.c(0.3, 0.3, 0.3, 1);
                }
                
                dui.box_colors(x + 0.01, y + 0.01, s - 0.02, s - 0.02, 0.0, bg_color, fg_color);
                
                const tr = get_block_texture_offset(blocks_template[i].side_texture);
                
                render.draw_textured_rect_crop(Rect.c(x + 0.03, y + 0.03, s - 0.06, s - 0.06),
                                               Rect.c(tr[0], tr[1], tr[2] - tr[0], tr[3] - tr[1]),
                                               tile_tex, Color.white, 0.0);
                x += s;
            }
            
            render.camera = player.camera;
        }
    }
    
    
    
    { // raycast
        const camera_forward = Vec3_rotate(Vec3.c(0, 0, -1), player.camera.transform.rotation);
        
        var ray_pos = player.camera.transform.position;
        
        var dummy_block = Block{};
        //var prev_b: *Block = &dummy_block;
        var prev_chunk_pos = Vec3i{};
        var prev_ray_pos = Vec3i{};
        
        const player_block_pos = world_to_block_position(player.position);
        
        const advance = 0.01;
        var l: f32 = 0;
        while (l < 5.0) : (l += advance) {
            const ray_block_pos = world_to_block_position(ray_pos);
            
            if (ray_block_pos.z < 0 or ray_block_pos.z >= Chunk.height) {
                ray_pos = Vec3_add(ray_pos, Vec3_mul_F(camera_forward, advance));
                continue;
            }
            
            const chunk_pos = chunk_position_from_block_position(ray_block_pos);
            const chunk_r_pos = block_position_relative_to_chunk(ray_block_pos);
            
            const chunk_x = cast(u32, chunk_r_pos.x);
            const chunk_y = cast(u32, chunk_r_pos.y);
            const chunk_z = cast(u32, chunk_r_pos.z);
            
            var chunk: *Chunk = undefined;
            if (get_chunk_from_hash(&chunk_hash_table, chunk_pos.x, chunk_pos.y)) |c| {
                chunk = c;
            } else {
                continue;
            }
            const block_i = Chunk.block_i(chunk_x, chunk_y, chunk_z);
            var block = &chunk.blocks.data[block_i];
            
            if (block.id != 0) {
                if (input.mouseDown(.left)) {
                    const cx = chunk_pos.x;
                    const cy = chunk_pos.y;
                    
                    break_block(chunk, chunk_r_pos);
                    net.send_block_break_event(chunk, chunk_r_pos);
                    
                    break;
                }
                
                if (input.mouseDown(.middle)) {
                    player.selected_block = block.id;
                }
                
                if (input.mouseDown(.right) and !Vec3i_eql(player_block_pos, prev_ray_pos)) {
                    const cx = prev_chunk_pos.x;
                    const cy = prev_chunk_pos.y;
                    
                    var p_chunk: *Chunk = undefined;
                    if (get_chunk_from_hash(&chunk_hash_table, cx, cy)) |c| {
                        p_chunk = c;
                    } else {
                        continue;
                    }
                    
                    const r_pos = block_position_relative_to_chunk(prev_ray_pos);
                    place_block(p_chunk, .{.id = player.selected_block}, r_pos);
                    net.send_block_place_event(chunk, r_pos, player.selected_block);
                }
                break;
            }
            
            prev_chunk_pos.x = chunk_pos.x;
            prev_chunk_pos.y = chunk_pos.y;
            //prev_b = block;
            prev_ray_pos = ray_block_pos;
            ray_pos = Vec3_add(ray_pos, Vec3_mul_F(camera_forward, advance));
        }
    }
}
