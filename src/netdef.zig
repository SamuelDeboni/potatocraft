pub const EventType = packed enum(u8) {
    chunk_request,
    chunk_response,
    ping,
    pong,
    block_break,
    block_place,
};

pub const RawEvent = struct {
    len: u8 = 0,
    data: [63]u8 = [_]u8{0} ** 63,
};

pub const ChunkRequestEvent = packed struct {
    event_type: EventType = .chunk_request, // please dont be stupid
    sender: u8 = 0,
    chunk_pos_x: i32,
    chunk_pos_y: i32,
};

pub const BlockBreakEvent = packed struct {
    event_type: EventType = .block_break, // please dont be stupid
    sender: u8 = 0,
    chunk_pos_x: i32,
    chunk_pos_y: i32,
    block_pos_x: u8,
    block_pos_y: u8,
    block_pos_z: u16,
};

pub const BlockPlaceEvent = packed struct {
    event_type: EventType = .block_place, // please dont be stupid
    sender: u8 = 0,
    block_id: u16,
    chunk_pos_x: i32,
    chunk_pos_y: i32,
    block_pos_x: u8,
    block_pos_y: u8,
    block_pos_z: u16,
};
