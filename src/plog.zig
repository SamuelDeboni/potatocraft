const std = @import("std");

const Module = enum {
    client,
    server,
    render,
    game,
    potatel,
};

const LogType = enum {
    normal,
    err,
    fatal,
};

fn get_module_color(module: Module) []const u8 {
    switch (module) {
        .client => return "\x1B[32m",
        .server => return "\x1B[34m",
        .render => return "\x1B[31m",
        .game => return "\x1B[33m",
        .potatel => return "\x1B[35m",
    }
}

const nocolor = "\x1b[0m";

fn log(log_type: LogType, module: Module, comptime fmt: []const u8, args: anytype) void {
    
    if (@import("builtin").os.tag == .windows) {
        switch (log_type) {
            .normal => std.debug.print("[{}] ",   .{module}),
            .err    => std.debug.print("* [{}] ", .{module}),
            .fatal  => std.debug.print("! [{}] ", .{module}),
        }
    } else {
        switch (log_type) {
            .normal => std.debug.print("[{s}{}\x1B[0m] ",                  .{ get_module_color(module), module }),
            .err    => std.debug.print("\x1B[33m*\x1B[0m [{s}{}\x1B[0m] ", .{ get_module_color(module), module }),
            .fatal  => std.debug.print("\x1B[31m!\x1B[0m [{s}{}\x1B[0m] ", .{ get_module_color(module), module }),
        }
    }
    
    std.debug.print(fmt, args);
}

pub fn plog(module: Module, s: []const u8) void {
    log(.normal, module, "{s}", .{s});
}

pub fn logln(module: Module, s: []const u8) void {
    log(.normal, module, "{s}\n", .{s});
}

pub fn loglnfmt(module: Module, comptime _fmt: []const u8, args: anytype) void {
    const fmt = _fmt ++ "\n";
    log(.normal, module, fmt, args);
}

pub fn perr(module: Module, s: []const u8) void {
    log(.err, module, "{s}", .{s});
}

pub fn errln(module: Module, s: []const u8) void {
    log(.err, module, "{s}\n", .{s});
}

pub fn errlnfmt(module: Module, comptime _fmt: []const u8, args: anytype) void {
    const fmt = _fmt ++ "\n";
    log(.err, module, fmt, args);
}

pub fn pfatal(module: Module, s: []const u8) void {
    log(.fatal, module, "{s}", .{s});
}

pub fn fatalln(module: Module, s: []const u8) void {
    log(.fatal, module, "{s}\n", .{s});
}

pub fn fatallnfmt(module: Module, comptime _fmt: []const u8, args: anytype) void {
    const fmt = _fmt ++ "\n";
    log(.fatal, module, fmt, args);
}