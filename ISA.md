## PotatoCraft ISA

- 16 bit machine

#### Registers

| Register | Value (hex) |
|----------|-------------|
| zero     | 0           |
| one      | 1           |
| ic       | 2           |
| sp       | 3           |
| ra       | 4           |
| acc      | 5           |
| r0 - r9  | 10 - 19     |

#### Instructions

| 8   | opcodes (hex) | size |
|-----|---------------|------|
|nop  | 0             | 1    |
|ret  | 1             | 1    |

| 8   |   8    | opcodes (hex) | size |
|-----|--------|---------------|------|
|not  |   r    | 10            | 2    |
|movr |   r    | 11            | 2    |

| 8   |   8    |    8    | opcodes (hex) | size |
|-----|--------|---------|---------------|------|
|add  |   r    |    r    | 20            | 3    |
|sub  |   r    |    r    | 21            | 3    |
|mul  |   r    |    r    | 22            | 3    |
|div  |   r    |    r    | 23            | 3    |
|or   |   r    |    r    | 24            | 3    |
|and  |   r    |    r    | 25            | 3    |
|xor  |   r    |    r    | 26            | 3    |

| 8    |   16   | opcodes (hex) | size |
|------|--------|---------------|------|
|inc   |   i    | 30            | 3    |
|dec   |   i    | 31            | 3    |
|mula  |   i    | 32            | 3    |
|diva  |   i    | 33            | 3    |

| 8   |   8    |    8    | opcodes (hex) | size |
|-----|--------|---------|---------------|------|
|sl   |   r    | r or bi | 40 - 41       | 3    |
|srl  |   r    | r or bi | 42 - 43       | 3    |
|sra  |   r    | r or bi | 44 - 45       | 3    |

| 8   |   8    |    8    | opcodes (hex) | size |
|-----|--------|---------|---------------|------|
|load | r or i | bi or i | 50 - 51       | 3    |
|store| r or i | bi or i | 52 - 53       | 3    |

| 8   |   8    |    8    | opcodes (hex) | size |
|-----|--------|---------|---------------|------|
|mova | r or i | 0 or i  | 60 - 61       | 3    |
|push | r or i | 0 or i  | 62 - 63       | 3    |
|pop  | r or i | 0 or i  | 64 - 65       | 3    |
|cmp  | r or i | 0 or i  | 66 - 67       | 3    |
|jmp  | r or i | 0 or i  | 68 - 69       | 3    |
|je   | r or i | 0 or i  | 6A - 6B       | 3    |
|jne  | r or i | 0 or i  | 6C - 6D       | 3    |
|jgt  | r or i | 0 or i  | 6E - 6F       | 3    |
|jlt  | r or i | 0 or i  | 70 - 71       | 3    |
|call | r or i | 0 or i  | 72 - 73       | 3    |
|sleep| r or i | 0 or i  | 74 - 75       | 3    |


##### examples

```
    jmp MAIN # jump to main

DATA:
    .data d42, d87, "asdfadsf", x0

VAR_YAY:
    .data x3, "yay"

MAIN:
    # load 42 to r1
    mova d42
    movr r1

    # load 87 to r2
    mova d87
    movr r2

    call ADD

    jmp MAIN

ADD:
    add  r1 r2
    movr r0
    ret
```

Basic program
```
00h | 0x69 0x03 0x00        jmp MAIN
                        MAIN:
03h | 0x61 0x05 0x00        mova d5
06h | 0x11 0x10             movr r0

08h | 0x61 0x05 0x00        mova d5
0Bh | 0x11 0x11             movr r1
                        LOOP:
0Dh | 0x22 0x11 0x11        mul  r1 r1
10h | 0x11 0x11             movr r1

12h | 0x21 0x10 0x01        sub  r0 one
15h | 0x67 0x10 0x00        cmp  r0 zero
18h | 0x6D 0x0F 0x00        je   LOOP
                        END:
1Bh | 0x69 0x1B 0x00        jmp  END
```





